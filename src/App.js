import React, { Component } from 'react';
import axios from "axios";

class App extends Component {
  state = {
    heading: "API USING AXIOS AND COMPONENTDIDMOUNT AND CLASS COMPONENT",
    todos: [],
  }
  componentDidMount() {
    this.fetchData();
  }
  fetchData() {
    axios.get(`https://jsonplaceholder.typicode.com/todos`)
      .then(response => {
        console.log("success", response.data)
        this.setState({
          todos: response.data,
        })
      })
      .catch(error => {
        console.log(error);
      })
  }
  render() {
    return (
      <>
        {this.state.heading}
        {this.state.todos && this.state.todos.map(item => {
          return <div key={item.id}>{item.title}</div>
        })}
      </>
    );
  }
}

export default App;